const members = [
    {
        name: 'Berk Uğuroğlu', 'roles': [
            "Otonom & Self Driving Takım Kaptanı",
        ],
        dep: "Bilgisayar Mühendisliği",
        img: "berk.jpeg",
        linkedin: "",
        inst: "berk.morrigan"

    },
    {
        name: 'Oğuzhan Akış', 'roles': [
            "Topluluk Başkanı",
        ],
        dep: "Makine Mühendisliği",
        img: "oguz.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Hasan Hüseyin Burhan', 'roles': [
            "Topluluk ve Mekanik Kaptanı",
        ],

        dep: "Makine Mühendisliği",
        img: "hasan.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Hacer Rana Karakaya', 'roles': [
            "Ön & Arka Düzen Kaptanı",
            
        ],
        dep: "Makine Mühendisliği",
        img: "rana.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Berkay Bolaç', 'roles': [
            "Otonom & Self Driving İkinci Kaptanı",
            
        ],
        dep: "Bilgisayar Mühendisliği",
        img: "berkay.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Ahmet Aydoğan', 'roles': [
            "Sponsorluk Kaptanı",
            
        ],
        dep: "Makine Mühendisliği",
        img: "ahmet.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Furkan Altunay', 'roles': [
            "Griffin UAV Takım Kaptanı",
        ],
        dep: "Makine Mühendisliği",
        img: "furkan.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Samet Cömert', 'roles': [
            "Merküt IHA Takım Kaptanı",
        ],
        dep: "Elektrik Elektronik Mühendisliği",
        img: "samet.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Muhammed Ali Çınar', 'roles': [
            "Batarya Yönetim Sistemi Kaptanı",
        ],
        dep: "Makine Mühendisliği",
        img: "muho.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Göktürk Kocabaş', 'roles': [
            "Elektrik Sistemleri Kaptanı",
        ],
        dep: "Elektrik Elektronik Mühendisliği",
        img: "gokturk.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Betül Bedenli', 'roles': [
            "Motor Ekibi Kaptanı",
        ],
        dep: "Makine Mühendisliği",
        img: "betul.jpeg",
        linkedin: "",
        inst: ""

    },
    {
        name: 'Berat Boynueğri', 'roles': [
            "Direksiyon Ekibi Kaptanı",
        ],
        dep: "Elektrik Elektronik Mühendisliği",
        img: "berat.jpeg",
        linkedin: "",
        inst: ""

    },


]
export default members;