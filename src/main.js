import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueParticlesBg from "particles-bg-vue";
import VueRouter from "vue-router";
import HelloWorld from "./components/HelloWorld.vue";
import Team from "./components/Team.vue";
import Press from "./components/Press.vue";
import Projects from "./components/Projects.vue";
import Join from "./components/Join.vue";





Vue.use(VueRouter);


Vue.config.productionTip = false


const router = new VueRouter(
  {
    routes: [{
      path: "/", component: HelloWorld

    },
    {
      path: "/team", component: Team
    },
    {
      path: "/press", component: Press
    },
    {
      path: "/projects", component: Projects
    },
    {
      path: "/joinus", component: Join
    }
    ],
    mode: "history"

  }
)


new Vue({
  vuetify,
  router,
  VueParticlesBg,
  render: h => h(App)
}).$mount('#app')
